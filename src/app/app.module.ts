import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TabledataComponent } from './tabledata/tabledata.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarModule } from 'ng-sidebar';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TabledataComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    SidebarModule.forRoot(),
    FormsModule,
    NgxDatatableModule,
    HttpModule,
    HttpClientModule
  ],
 bootstrap: [AppComponent]
})
export class AppModule { }
